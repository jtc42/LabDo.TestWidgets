# -*- coding: utf-8 -*-
"""
ConsoleWidget is used to allow execution of user-supplied python commands
in an application. It also includes a command history and functionality for trapping
and inspecting stack traces.

"""
import pyqtgraph as pg
from PyQt5 import QtCore, QtGui
import numpy as np

from widgets import console

#app = pg.mkQApp()
app = QtGui.QApplication([])


## build an initial namespace for console commands to be executed in (this is optional;
## the user can always import these modules manually)
class MyClass:
    def __init__(self, i):
        self.val = i
		
    def doubleval(self):
        self.val = self.val*2
	
    def crash(self):
        raise Exception("Broke")

    def __enter__(self):
        print("in __enter__")
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        # never called because
        # argument signature is wrong
        print("in __exit__")
		
with MyClass(5) as myinstance:
	
	namespace = {'pg': pg, 'np': np, 'myinstance': myinstance}
	
	print("Initial value: {}".format(myinstance.val))

	## initial text to display in the console
	text = """
	This is an interactive python console. The numpy and pyqtgraph modules have already been imported 
	as 'np' and 'pg'. 

	Go, play.
	"""
	c = console.ConsoleWidget(namespace=namespace, text=text)

	c.show()
	c.setWindowTitle('pyqtgraph example: ConsoleWidget')

	QtGui.QApplication.instance().exec_()

	print("Closing value: {}".format(myinstance.val))